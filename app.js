VIEW_PRESETS = {
  default: [-1.5, 5, 1.5],
  // y=0 rotates around "up" axis, so set y=1e-7
  top: [0, 1e-7, 5],
  front: [-5, 0, 0],
  side: [0, 5, 0],
};

// Utility functions

function interpolate(a, b, perc) {
  return a + (b - a) * perc;
}

function interpolatePoint(p1, p2, perc) {
  var pi = [];
  for (var i = 0; i < p1.length; i++) {
    pi[i] = interpolate(p1[i], p2[i], perc);
  }
  return pi;
}

function interpolateFrom(value, old_min, old_max, new_min, new_max) {
  var perc = (value - old_min) / (old_max - old_min);
  return interpolate(new_min, new_max, perc);
}

function interpolatePointFrom(point, old_min, old_max, new_min, new_max) {
  var pi = [];
  for (var i = 0; i < point.length; i++) {
    pi[i] = interpolate(point[i], old_min[i], old_max[i], new_min[i], new_max[i]);
  }
  return pi;
}

function scaleTowards(value, middle, perc) {
  if (value == middle)
    return value;
  return interpolateFrom(value, middle, value, middle, (value - middle) * perc + middle);
}

function midpoint(p1, p2) {
  var m = [];
  for (var i = 0; i < 3; i++) {
    m.push((p1[i] + p2[i]) / 2);
  }
  return m;
}

function pointDistance(p1, p2) {
  return Math.sqrt(
    (p1[0] - p2[0]) * (p1[0] - p2[0]) +
    (p1[1] - p2[1]) * (p1[1] - p2[1]) +
    (p1[2] - p2[2]) * (p1[2] - p2[2])
  );
}

function makeLine(linePoints, material) {
  var geometry = new THREE.Geometry();
  linePoints.forEach(function(p) {
    geometry.vertices.push(new THREE.Vector3(p[0], p[1], p[2]));
  });
  var line = new THREE.Line(geometry, material);
  return line;
}

function getMeshPoint(mesh, i, j) {
  // i = row, j = column (i = x index, j = y index, not coordinates)
  // allows negative i and j to index from end of mesh for convenience
  if (i < 0)
    i += mesh.length;
  if (j < 0)
    j += mesh[i].length;
  return mesh[i][j];
}

function forEachMeshPoint(mesh, callback) {
  // callback(point, i, j)
  for (var i = 0; i < mesh.length; i++) {
    for (var j = 0; j < mesh[i].length; j++) {
      callback(mesh[i][j], i, j);
    }
  }
}

function findMeshMaxAbsCoord(mesh) {
  var max = 0;
  forEachMeshPoint(mesh, function(p) {
    for (var k = 0; k < 3; k++) {
      max = Math.max(max, Math.abs(p[k]));
    }
  });
  return max;
}

function findMeshAbsBounds(mesh) {
  var bounds = {
    min_x: Infinity,
    min_y: Infinity,
    min_z: Infinity,
    max_x: 0,
    max_y: 0,
    max_z: 0,
  }
  forEachMeshPoint(mesh, function(p) {
    bounds.min_x = Math.min(bounds.min_x, Math.abs(p[0]));
    bounds.min_y = Math.min(bounds.min_y, Math.abs(p[1]));
    bounds.min_z = Math.min(bounds.min_z, Math.abs(p[2]));
    bounds.max_x = Math.max(bounds.max_x, Math.abs(p[0]));
    bounds.max_y = Math.max(bounds.max_y, Math.abs(p[1]));
    bounds.max_z = Math.max(bounds.max_z, Math.abs(p[2]));
  });
  return bounds;
}

function newEmptyMesh(i_dim, j_dim) {
  var mesh = [];
  for (var i = 0; i < i_dim; i++) {
    mesh.push([]);
    for (var j = 0; j < j_dim; j++) {
      mesh[i].push([]);
    }
  }
  return mesh;
}

function newEmptyMeshFrom(mesh) {
  return replaceMesh(mesh, function() {
    return [];
  });
}

function newRectangularMesh(x_cells, y_cells, x_max, y_max) {
  var mesh = newEmptyMesh(x_cells + 1, y_cells + 1);
  for (var i = 0; i <= x_cells; i++) {
    for (var j = 0; j <= y_cells; j++) {
      mesh[i][j] = [
        (i * 2 * x_max / x_cells) - x_max,
        j * y_max / y_cells,
        0
      ];
    }
  }
  return mesh;
}

function cloneMesh(mesh) {
  var new_mesh = newEmptyMeshFrom(mesh);
  forEachMeshPoint(mesh, function(p, i, j) {
    new_mesh[i][j] = p.slice();
  });
  return new_mesh;
}

function replaceMesh(mesh, callback) {
  // callback(point, i, j)
  var new_mesh = [];
  for (var i = 0; i < mesh.length; i++) {
    new_mesh.push([]);
    for (var j = 0; j < mesh[i].length; j++) {
      new_mesh[i].push(callback(mesh[i][j], i, j));
    }
  }
  return new_mesh;
}

function isMeshSymmetric(mesh) {
  for (var i = 0; i < mesh.length; i++) {
    for (var j = 0; j < mesh[i].length / 2; j++) {
      var p1 = mesh[i][j];
      var p2 = mesh[i][mesh[i].length - 1 - j];
      if (p1[0] != p2[0] || p1[1] != -p2[1] || p1[2] != p2[2])
        return false;
    }
  }
  return true;
}

function isMeshOneSided(mesh) {
  var first_y = mesh[0][0][1];
  for (var i = 0; i < mesh.length; i++) {
    for (var j = 0; j < mesh[i].length; j++) {
      if (mesh[i][j][1] * first_y < 0) {
        // opposite signs of y coordinates
        return false;
      }
    }
  }
  return true;
}

function joinMeshParts(mesh_parts) {
  // copy entire first part
  var mesh = cloneMesh(mesh_parts[0]);

  // add remaining parts
  for (var p = 1; p < mesh_parts.length; p++) {
    // we need to shift later parts of the mesh - do this by calculating the
    // distance between the midpoints of the lines at the end of both parts,
    // which should overlap if no transformations had occurred, then shift this
    // part by that distance

    // midpoint of first column of this part
    var cur_midpoint = midpoint(getMeshPoint(mesh_parts[p], 0, 0), getMeshPoint(mesh_parts[p], -1, 0));
    // midpoint of last column of previous part
    var prev_midpoint = midpoint(getMeshPoint(mesh, 0, -1), getMeshPoint(mesh, -1, -1));

    // amount to scale this part in the x and z directions to match with previous part
    var scale = pointDistance(getMeshPoint(mesh_parts[p-1], 0, -1), getMeshPoint(mesh_parts[p-1], -1, -1)) /
                pointDistance(getMeshPoint(mesh_parts[p], 0, 0), getMeshPoint(mesh_parts[p], -1, 0));

    // first column in the final mesh that will be from this part
    var first_j = mesh[0].length;
    for (var i = 0; i < mesh.length; i++) {
      // skip overlapping columns (skip j=0 for sections past first section)
      for (var j = 1; j < mesh_parts[p][i].length; j++) {
        var new_point = [];
        for (var k = 0; k < 3; k++) {
          new_point.push(mesh_parts[p][i][j][k] - cur_midpoint[k] + prev_midpoint[k]);
        }
        mesh[i].push(new_point);
      }
    }

    // scale newly-added columns
    for (var j = first_j; j < mesh[0].length; j++) {
      var j_midpoint = midpoint(getMeshPoint(mesh, 0, j), getMeshPoint(mesh, -1, j));
      for (var i = 0; i < mesh.length; i++) {
        mesh[i][j][0] = scaleTowards(mesh[i][j][0], j_midpoint[0], scale);
        mesh[i][j][2] = scaleTowards(mesh[i][j][2], j_midpoint[2], scale);
      }
    }
  }
  return mesh;
}

function findMeshPartWithColumn(mesh_parts, j) {
  // j is the index of the column (the line in the "y" dimension)
  var cur_part = null;
  var column_index = j;
  var part_index;
  for (var i = 0; i < mesh_parts.length; i++) {
    // the last line of each part overlaps with the first of the next part,
    // so ignore the last line of the current part
    var num_cols = mesh_parts[i][0].length - 1;
    if (column_index < num_cols) {
      cur_part = mesh_parts[i];
      part_index = i;
      break;
    }
    else {
      column_index -= num_cols;
    }
  }
  return {
    part: cur_part,
    part_index: part_index,
    column_index: column_index,
  };
}

// split a mesh section into two sections
// j_split is the index of the line to split on in the "y" dimension.
function splitMeshParts(mesh_parts, j_split) {
  // find the section in mesh_parts containing the j_split index
  var res = findMeshPartWithColumn(mesh_parts, j_split);
  if (res.column_index == 0) {
    // can't split on edge
    return {
      ok: false,
      error: 'This line is the end of a section - cannot split here',
    };
  }
  var old_part = res.part;
  var new_part = newEmptyMesh(old_part.length, old_part[0].length - res.column_index);
  for (var i = 0; i < old_part.length; i++) {
    for (var j = res.column_index; j < old_part[i].length; j++) {
      new_part[i][j - res.column_index] = old_part[i][j];
    }
  }
  for (var i = 0; i < old_part.length; i++) {
    old_part[i].length = res.column_index + 1;
  }
  // insert new part after old part
  mesh_parts.splice(res.part_index + 1, 0, new_part);
  return {
    ok: true,
    part_indices: [res.part_index, res.part_index + 1],
  };
}

// return a NPY file in a Uint8Array
function meshToNPY(mesh) {
  // generate header - magic string, then version number (1)
  var magic = '\x93NUMPY\x01\x00';
  var info = "{'descr': '<f8', 'fortran_order': False, 'shape': (XX, YY, 3), }";
  info = info.replace('XX', mesh.length).replace('YY', mesh[0].length);
  // numpy includes extra spaces at the end, so add an arbitrary number to imitate it
  info += ' '.repeat(16);
  // magic + info length must be a multiple of 8 to align data properly.
  // since magic is 8 characters already, just align info. Add 2 to include the length field before info.
  info += ' '.repeat(8 - ((info.length + 2) % 8));
  // append info length to magic (as a little-endian uint16)
  magic += String.fromCharCode(info.length % 256);
  magic += String.fromCharCode(Math.floor(info.length / 256));

  var header = magic + info;

  var flat_array = [];
  for (var i = 0; i < mesh.length; i++) {
    for (var j = 0; j < mesh[i].length; j++) {
      for (var k = 0; k < mesh[i][j].length; k++) {
        flat_array.push(mesh[i][j][k]);
      }
    }
  }

  var f64array = new Float64Array(mesh.length * mesh[0].length * 3);
  // if (f64array.length != flat_array.length) {
  //   throw "Irregular mesh dimensions: " + f64array.length + ' != ' + flat_array.length;
  // }
  for (var i = 0; i < f64array.length; i++) {
    f64array[i] = flat_array[i];
  }
  // individual bytes of f64array
  var u8array = new Uint8Array(f64array.buffer);

  var full_array = new Uint8Array(header.length + u8array.length);
  for (var i = 0; i < header.length; i++) {
    full_array[i] = header.charCodeAt(i);
  }
  // copy u8array into full_array after header
  full_array.set(u8array, header.length);

  return full_array;
}

var meshModifiers = {
  x_cells: function(mesh, new_x) {
    if (new_x < 1)
      return mesh;
    var new_mesh = [];
    for (var i = 0; i <= new_x; i++) {
      new_mesh.push([]);
    }
    for (var y = 0; y < mesh[0].length; y++) {
      for (var x = 0; x <= new_x; x++) {
        new_mesh[x].push(interpolatePoint(
          mesh[0][y],
          mesh[mesh.length - 1][y],
          x / new_x
        ));
      }
    }
    return new_mesh;
  },
  y_cells: function(mesh, new_y) {
    if (new_y < 1)
      return mesh;
    var new_mesh = [];
    for (var x = 0; x < mesh.length; x++) {
      var row = [];
      for (var y = 0; y <= new_y; y++) {
        row.push(interpolatePoint(
          mesh[x][0],
          mesh[x][mesh[x].length - 1],
          y / new_y
        ));
      }
      new_mesh.push(row);
    }
    return new_mesh;
  },
  stretch: function(mesh, ratio) {
    var bounds = findMeshAbsBounds(mesh);
    var new_max_y = bounds.min_y + (ratio * (bounds.max_y - bounds.min_y));
    return replaceMesh(mesh, function(p) {
      return [
        p[0],
        interpolateFrom(p[1], bounds.min_y, bounds.max_y, bounds.min_y, new_max_y),
        p[2]
      ];
    });
  },
  taper: function(mesh, taper) {
    if (taper == 1)
      return mesh;
    var bounds = findMeshAbsBounds(mesh);
    var new_x_bounds = mesh[0].map(function(point, j) {
      var x1 = mesh[0][j][0];
      var x2 = mesh[mesh.length - 1][j][0];
      var dx = (x2 - x1) / 2;
      var cur_scale = interpolateFrom(Math.abs(point[1]), bounds.min_y, bounds.max_y, 1, taper);
      x1 = x1 + dx - cur_scale*dx;
      x2 = x2 - dx + cur_scale*dx;
      return [x1, x2];
    });
    return replaceMesh(mesh, function(p, i, j) {
      return [
        interpolate(new_x_bounds[j][0], new_x_bounds[j][1], i / (mesh.length - 1)),
        p[1],
        p[2]
      ];
    });
    return new_mesh;
  },
  sweep: function(mesh, sweep) {
    if (sweep == 0 || Math.abs(sweep) >= 90)
      return mesh;
    var bounds = findMeshAbsBounds(mesh);
    // maximum x distance this segment can move (farthest from y=min_y)
    var max_dx = Math.tan(sweep * Math.PI / 180) * (bounds.max_y - bounds.min_y);
    return replaceMesh(mesh, function(p) {
      return [
        p[0] + interpolateFrom(Math.abs(p[1]), bounds.min_y, bounds.max_y, 0, max_dx),
        p[1],
        p[2]
      ];
    });
  },
  dihedral: function(mesh, dihedral) {
    if (dihedral == 0 || Math.abs(dihedral) >= 90)
      return mesh;
    var bounds = findMeshAbsBounds(mesh);
    // maximum vertical distance this segment can move (farthest from y=min_y)
    var max_dz = Math.tan(dihedral * Math.PI / 180) * (bounds.max_y - bounds.min_y);
    return replaceMesh(mesh, function(p) {
      return [
        p[0],
        p[1],
        p[2] + interpolateFrom(Math.abs(p[1]), bounds.min_y, bounds.max_y, 0, max_dz)
      ];
    });
  },
  twist: function(mesh, angle) {
    if (angle == 0)
      return mesh;
    angle = angle * Math.PI / 180; // deg to rad
    var max_y = findMeshAbsBounds(mesh).max_y;
    var new_mesh = newEmptyMeshFrom(mesh);
    for (var j = 0; j < mesh[0].length; j++) {
      var mid_x = (mesh[0][j][0] + mesh[mesh.length - 1][j][0]) / 2;
      var mid_z = (mesh[0][j][2] + mesh[mesh.length - 1][j][2]) / 2;
      for (var i = 0; i < mesh.length; i++) {
        var cur_y = mesh[i][j][1];
        var cur_shift_angle = interpolate(angle, 0, Math.abs(mesh[i][j][1] / max_y));
        var norm_x = mesh[i][j][0] - mid_x;
        var norm_z = mesh[i][j][2] - mid_z;
        if (norm_x == 0 && norm_z == 0) {
          // rotations can move points at the center of rotation to weird
          // places, but they don't need to move, so just copy the original
          // point instead
          new_mesh[i][j] = mesh[i][j].slice();
          continue;
        }
        var radius = Math.sqrt(norm_x*norm_x + norm_z*norm_z);
        var cur_angle = Math.atan(norm_x / norm_z);
        if (norm_z < 0)
          cur_angle += Math.PI;
        var new_x = radius * Math.sin(cur_angle + cur_shift_angle) + mid_x;
        var new_z = radius * Math.cos(cur_angle + cur_shift_angle) + mid_z;
        new_mesh[i][j] = [new_x, cur_y, new_z];
      }
    }
    return new_mesh;
  },
}; //meshModifiers

function defaultMeshSettings(length) {
  var defaults = {
    x_cells: 0,
    y_cells: 0,
    stretch: 1,
    taper: 1,
    sweep: 0,
    dihedral: 0,
    twist: 0,
  };
  if (!length)
    return defaults;
  var arr = [];
  for (var i = 0; i < length; i++) {
    arr.push(defaultMeshSettings());
  }
  return arr;
}

function defaultPlotSettings() {
  return {
    flip: {
      x: false,
      y: false,
      z: false,
    },
    showGrid: true,
    highlight: {},
  };
}

function newMeshUI() {
  return {
    x_cells: 4,
    y_cells: 5,
    x_max: 1,
    y_max: 5,
    mirror: false,
  };
}

var app = new Vue({
  el: '#app',
  data: {
    graphics: {},
    orig_mesh: null,
    // before any transformations
    orig_mesh_parts: [],
    // after all transformations
    cur_mesh_parts: [],
    mesh_props: {},
    mesh_mirror: false,
    mesh_settings: defaultMeshSettings(1),
    plot_settings: defaultPlotSettings(),
    sampleMeshId: '',
    in_split: false,
    split_error: '',
    new_mesh_ui: newMeshUI(),
    in_new_mesh: false,
  },
  methods: {
    loadMesh: function(mesh) {
      this.orig_mesh = mesh;
      this.mesh_props = {};

      var mesh_scale = this.graphics.meshGroup.scale;
      var max = findMeshMaxAbsCoord(mesh);
      mesh_scale.x = mesh_scale.y = mesh_scale.z = 1 / max;

      var is_symmetric = isMeshSymmetric(mesh);
      this.mesh_props.asymmetric = !is_symmetric && !isMeshOneSided(mesh);
      if (is_symmetric) {
        // remove half, and turn on mirroring
        this.orig_mesh = this.orig_mesh.map(function(row) {
          return row.filter(function(point) {
            return point[1] <= 0;
          });
        });
        this.mesh_mirror = true;
      }
      else {
        this.mesh_mirror = false;
      }
      this.orig_mesh.forEach(function(row) {
        // normalize so row starts closer to y=0
        if (Math.abs(row[0][1]) >= Math.abs(row[row.length - 1][1])) {
          row.reverse();
        }
      });
      this.orig_mesh_parts = [cloneMesh(this.orig_mesh)];

      this.resetMesh();
      this.drawMesh(mesh);
    }, // loadMesh

    createNewMesh: function() {
      this.loadMesh(newRectangularMesh(
        this.new_mesh_ui.x_cells,
        this.new_mesh_ui.y_cells,
        this.new_mesh_ui.x_max,
        this.new_mesh_ui.y_max
      ));
      this.mesh_mirror = this.new_mesh_ui.mirror;
    },

    drawMesh: function(mesh) {
      var g = this.graphics;

      for (var i = g.meshGroup.children.length - 1; i >= 0; i--) {
        g.meshGroup.remove(g.meshGroup.children[i]);
      }
      for (var i = g.highlightGroup.children.length - 1; i >= 0; i--) {
        g.highlightGroup.remove(g.highlightGroup.children[i]);
      }

      var i_len = mesh.length;
      var j_len = mesh[0].length;
      // make side-to-side lines (chords)
      for (var i = 0; i < i_len; i++) {
        var linePoints = [];
        for (var j = 0; j < j_len; j++) {
          mesh[i][j] = mesh[i][j];
          linePoints.push(mesh[i][j]);
        }
        var line = makeLine(linePoints, g.line_material);
        line.mesh_dir = 'y';
        line.mesh_index = i;
        g.meshGroup.add(line);
      }
      // make front-to-back lines
      for (var j = 0; j < j_len; j++) {
        var linePoints = [];
        for (var i = 0; i < i_len; i++) {
          linePoints.push(mesh[i][j]);
        }
        var line = makeLine(linePoints, g.line_material);
        line.mesh_dir = 'x';
        line.mesh_index = j;
        if (this.mesh_mirror) {
          line.mesh_index -= Math.floor(j_len / 2);
        }
        g.meshGroup.add(line);
      }

      // apply plot settings
      ['x', 'y', 'z'].forEach(function(axis) {
        g.meshGroup.scale[axis] = Math.abs(g.meshGroup.scale[axis]) *
          (this.plot_settings.flip[axis] ? -1 : 1);
      }.bind(this));

      g.grid.visible = this.plot_settings.showGrid;

      for (var i = 0; i < this.cur_mesh_parts.length; i++) {
        var part = this.cur_mesh_parts[i];
        if (this.plot_settings.highlight[i]) {
          var points = [
            getMeshPoint(part, 0, 0),
            getMeshPoint(part, 0, -1),
            getMeshPoint(part, -1, -1),
            getMeshPoint(part, -1, 0),
            getMeshPoint(part, 0, 0),
          ];
          for (var j = 0; j < points.length - 1; j++) {
            g.highlightGroup.add(makeLine(points.slice(j, j + 2), g.highlight_material));
            if (this.mesh_mirror) {
              var mirrored_line = makeLine(points.slice(j, j + 2), g.highlight_material);
              mirrored_line.scale.y = -1;
              g.highlightGroup.add(mirrored_line);
            }
          }
        }
      }
      ['x', 'y', 'z'].forEach(function(axis) {
        g.highlightGroup.scale[axis] = g.meshGroup.scale[axis];
      });
    }, // drawMesh

    joinMesh: function(mesh_parts) {
      var mesh = joinMeshParts(mesh_parts);

      // add mirrored half of mesh
      if (this.mesh_mirror) {
        mesh = mesh.map(function(row) {
          var y_start_0 = (row[0][1] == 0);
          var mirrored_points = [];
          row.forEach(function(point) {
            if (point[1] != 0) {
              mirrored_points.push([point[0], -point[1], point[2]]);
            }
          });
          // detect which end has y=0
          if (row[0][1] == 0) {
            mirrored_points.reverse();
            return mirrored_points.concat(row);
          }
          else {
            return row.concat(mirrored_points);
          }
        });
      }

      return mesh;
    },

    updateMesh: function() {
      while (this.mesh_settings.length < this.orig_mesh_parts.length) {
        this.mesh_settings.push(defaultMeshSettings());
      }

      var defaults = defaultMeshSettings();
      this.cur_mesh_parts = this.orig_mesh_parts.map(function(part, i) {
        var settings = this.mesh_settings[i];

        if (settings.x_cells != part.length - 1) {
          part = meshModifiers.x_cells(part, settings.x_cells);
        }
        if (settings.y_cells != part[0].length - 1) {
          part = meshModifiers.y_cells(part, settings.y_cells);
        }

        ['stretch', 'taper', 'sweep', 'dihedral', 'twist'].forEach(function(id) {
          if (defaults[id] != settings[id])
            part = meshModifiers[id](part, settings[id]);
        }.bind(this));

        return part;
      }.bind(this));

      this.cur_mesh = this.joinMesh(this.cur_mesh_parts);
      this.drawMesh(this.cur_mesh);
      this.checkYCells();
    }, // updateMesh

    download: function() {
      var u8array = meshToNPY(this.joinMesh(this.cur_mesh_parts));
      var blob = new Blob([u8array], {type: 'application/octet-stream'});
      var url = window.URL.createObjectURL(blob);
      var link = this.$refs.downloadLink;
      link.href = url;
      link.download = 'mesh.npy';
      link.click();
    },

    resetView: function() {
      this.graphics.controls.reset();
      this.plot_settings = defaultPlotSettings();
      this.viewPreset('default');
    },

    resetMesh: function() {
      this.mesh_settings = defaultMeshSettings(this.orig_mesh_parts.length);
      this.orig_mesh_parts.forEach(function(part, i) {
        this.mesh_settings[i].x_cells = part.length - 1;
        this.mesh_settings[i].y_cells = part[0].length - 1;
      }.bind(this));
    },

    viewPreset: function(name) {
      if (!VIEW_PRESETS[name])
        throw "Invalid preset: " + name;
      var position = this.graphics.camera.position;
      position.set.apply(position, VIEW_PRESETS[name]);
      this.graphics.controls.update();
    },

    findLineFromMouse: function(client_x, client_y, mesh_dir) {
      var g = this.graphics;
      var container = this.$refs.plotContainer;

      var mouse = new THREE.Vector3();
      mouse.x = 2 * (client_x / container.clientWidth) - 1;
      mouse.y = -2 * (client_y / container.clientHeight) + 1;

      g.raycaster.linePrecision = 0.01 / this.graphics.meshGroup.scale.x;
      g.raycaster.setFromCamera(mouse, this.graphics.camera);

      var intersects = g.raycaster.intersectObjects(g.meshGroup.children);
      for (var i = 0; i < intersects.length; i++) {
        if (mesh_dir && intersects[i].object.mesh_dir != mesh_dir)
          continue;
        return intersects[i].object;
      }
    },

    startSplit: function() {
      this.in_split = true;
      this.split_error = '';
      this.checkYCells();
    },

    stopSplit: function() {
      this.in_split = false;
      this.split_error = '';
    },

    checkYCells: function() {
      this.split_impossible_y_cells = false;
      for (var i = 0; i < this.orig_mesh_parts.length; i++) {
        if (this.orig_mesh_parts[i][0].length - 1 != this.mesh_settings[i].y_cells) {
          this.split_impossible_y_cells = true;
          break;
        }
      }
    },

    resetYCells: function() {
      for (var i = 0; i < this.orig_mesh_parts.length; i++) {
        this.mesh_settings[i].y_cells = this.orig_mesh_parts[i][0].length - 1;
      }
      this.split_impossible_y_cells = false;
    },

    onWindowResize: function() {
      var container = this.$refs.plotContainer;
      var camera = this.graphics.camera;

      camera.aspect = container.clientWidth / container.clientHeight;
      camera.updateProjectionMatrix();

      this.graphics.renderer.setSize(container.clientWidth, container.clientHeight);
    }, // onWindowResize

    onMouseMove: function(e) {
      if (!this.in_split || this.split_impossible_y_cells)
        return;

      var g = this.graphics;

      g.meshGroup.children.forEach(function(line) {
        line.material = g.line_material;
      }.bind(this));

      var line = this.findLineFromMouse(e.clientX, e.clientY, 'x');
      if (!line)
        return;
      line.material = g.line_highlight_material;
    },

    onClick: function(e) {
      if (!this.in_split || this.split_impossible_y_cells)
        return;

      var line = this.findLineFromMouse(e.clientX, e.clientY, 'x');
      if (!line)
        return;
      var index = Math.abs(line.mesh_index);

      this.split_error = '';
      var res = splitMeshParts(this.orig_mesh_parts, index);
      if (res.ok) {
        line.material = this.graphics.line_material;
        this.in_split = false;
        // copy mesh settings from old part
        this.mesh_settings.splice(res.part_indices[1], 0,
          Object.assign({}, this.mesh_settings[res.part_indices[0]]));
        // update y coordinates
        res.part_indices.forEach(function(i) {
          this.mesh_settings[i].y_cells = this.orig_mesh_parts[i][0].length - 1;
        }.bind(this));
        this.updateMesh();
      }
      else {
        this.split_error = res.error || 'Split failed (unknown error)';
      }
    },

    updateXcells: function(event) {
      this.mesh_settings.forEach(function(settings) {
        settings.x_cells = Number(event.target.value);
      });
    },

    render: function() {
      var g = this.graphics;
      // fallback if requestAnimationFrame isn't available
      if (window.requestAnimationFrame)
        requestAnimationFrame(this.render.bind(this));
      else
        setTimeout(this.render.bind(this), 1000/30);
      g.renderer.render(g.scene, g.camera);
    },

  },
  watch: {
    mesh_settings: {
      deep: true,
      handler: function() {
        this.updateMesh();
      },
    },
    mesh_mirror: function() {
      this.updateMesh();
    },
    plot_settings: {
      deep: true,
      handler: function() {
        this.drawMesh(this.cur_mesh);
      },
    },
    sampleMeshId: function() {
      if (this.sampleMeshId != '') {
        this.loadMesh(SAMPLE_MESHES[this.sampleMeshId], true);
        this.sampleMeshId = '';
      }
    },
  },
  mounted: function() {
    this.$refs.sidebar.classList.remove('loading');
    // set up canvas and three.js objects
    var g = this.graphics;
    var container = this.$refs.plotContainer;

    g.camera = new THREE.PerspectiveCamera(25, container.clientWidth / container.clientHeight, 0.1, 1000);
    g.camera.up.set(0, 0, 1);
    g.camera.position.set.apply(g.camera.position, VIEW_PRESETS.default);

    g.raycaster = new THREE.Raycaster();

    g.controls = new THREE.OrbitControls(g.camera, container);
    g.controls.enableKeys = false;

    g.scene = new THREE.Scene();
    g.scene.background = new THREE.Color(0xcccccc);

    g.meshGroup = new THREE.Group();
    g.scene.add(g.meshGroup);

    g.highlightGroup = new THREE.Group();
    g.highlight_material = new THREE.LineBasicMaterial({color: 0x66ff00, linewidth: 3});
    g.scene.add(g.highlightGroup);

    g.line_material = new THREE.LineBasicMaterial({color: 0x000000});
    g.line_highlight_material = new THREE.LineBasicMaterial({color: 0xff0000});

    g.grid = new THREE.GridHelper(2, 20, 0x8888aa, 0xaaaacc);
    g.scene.add(g.grid);

    g.renderer = new THREE.CanvasRenderer();
    g.renderer.setPixelRatio(window.devicePixelRatio);
    g.renderer.setSize(container.clientWidth, container.clientHeight);

    container.appendChild(g.renderer.domElement);

    window.addEventListener('resize', this.onWindowResize.bind(this), false);
    container.addEventListener('mousemove', this.onMouseMove.bind(this), false);
    container.addEventListener('click', this.onClick.bind(this), false);
    this.render();

    this.loadMesh(SAMPLE_MESHES[SAMPLE_MESHES.length - 1]);
  },
});
