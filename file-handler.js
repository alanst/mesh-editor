// Based loosely on https://gist.github.com/nvictus/88b3b5bfe587d32ac1ab519fd0009607

// read little-endian 16-bit unsigned int
Uint8Array.prototype.getUint16LE = function(i) {
    return this[i] | (this[i+1] << 8);
};
// read ASCII string with specific length
Uint8Array.prototype.getString = function(i, length) {
    return String.fromCharCode.apply(null, this.slice(i, i + length));
};

function groupArray(array, group_size) {
    var out = []
    for (var i = 0; i < array.length; i += group_size) {
        out.push(array.slice(i, i + group_size));
    }
    return out;
}

fileHandler = {
    events: $({}),
    dataTypes: {
        "|u1": Uint8Array,
        "|i1": Int8Array,
        "<u2": Uint16Array,
        "<i2": Int16Array,
        "<u4": Uint32Array,
        "<i4": Int32Array,
        "<f4": Float32Array,
        "<f8": Float64Array,
    },
    onUpload: function() {
        if (!this.files.length)
            return;
        var file = this.files[0];
        $('.upload-wrapper span').text(file.name);
        var reader = new FileReader();
        reader.onload = fileHandler.onUploadComplete;
        reader.readAsArrayBuffer(file);
    },
    onUploadComplete: function(event) {
        function err(msg) {
            fileHandler.events.trigger('error', {message: msg});
        }
        buffer = event.target.result;
        u8 = new Uint8Array(buffer);
        if (u8.getString(1, 5) != 'NUMPY') {
            return err('Invalid file format (expected binary .npy file)');
        }
        var info_length = u8.getUint16LE(8);
        var info = u8.getString(10, info_length);
        var dims, type_str;
        try {
            dims = info.match(/shape.*?(\d+)[^\d]+?(\d+)[^\d]+?(\d+)/).slice(1).map(Number);
            type_str = info.match(/descr.+?['"](...)['"]/)[1];
        }
        catch(e) {
            return err('Failed to parse metadata');
        }
        if (dims[2] != 3) {
            return err('Expected 3D array with last dimension = 3, got ' + dims[2]);
        }
        var array_type = fileHandler.dataTypes[type_str];
        if (!array_type) {
            return err('Unsupported data type: ' + type_str);
        }
        var array = Array.from(new array_type(buffer, 10 + info_length));
        array = groupArray(array, dims[2]);
        array = groupArray(array, dims[1]);
        if (array.length != dims[0]) {
            return err('Malformed array - expected first dimension = ' + dims[0] + ', got ' + array.length);
        }
        fileHandler.events.trigger('upload', {array: array});
    },
};

$('body').on('click', '.upload-wrapper button', function() {
    $(this).siblings('input[type=file]')[0].click();
});
$('body').on('change', 'input[type=file]', fileHandler.onUpload);
fileHandler.events.on('upload', function(_, data) {
    app.loadMesh(data.array, true);
});
fileHandler.events.on('error', function(_, data) {
    alert('File upload error: ' + (data.message || 'unknown error'));
});
